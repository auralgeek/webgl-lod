// Configure require.js
var require = {
	baseUrl: 'js/app',
	shim: {
		'threejs': { exports: 'THREE' },
		'OrbitControls': { deps: ['threejs'], exports: 'THREE' },
		'FirstPersonControls': { deps: ['threejs'], exports: 'THREE' },
		'SkyShader': { deps: ['threejs'], exports: 'THREE' },
		'KeyboardState': { deps: ['threejs'], exports: 'THREEx' }
	},
	paths: {
		threejs: '../lib/three.min',
		OrbitControls: '../lib/OrbitControls',
		FirstPersonControls: '../lib/FirstPersonControls',
		SkyShader: '../lib/SkyShader',
		KeyboardState: '../lib/THREEx.KeyboardState'
	}
};