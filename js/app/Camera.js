define(['threejs', 'Scene'], function(THREE, _scene) {

    var WIDTH = window.innerWidth;
    var HEIGHT = window.innerHeight;
    _camera = new THREE.PerspectiveCamera(45, WIDTH / HEIGHT, 0.1, 1000000);
    _camera.position.z = 5;

    var updateAspect = function() {
	    _camera.aspect = WIDTH / HEIGHT;
        _camera.updateProjectionMatrix();
    }

    window.addEventListener( 'resize', updateAspect, false );
    updateAspect();

    //_scene.add(_camera);
    
    return _camera;
});