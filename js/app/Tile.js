define(['threejs', 'Material'], function(THREE, _material) {
  // Tile class

  // Constructor
  // We assume height data is an array arranged as:
  // 12, 13, 14, 15];  ^ increasing z
  //  8,  9, 10, 11,   |
  //  4,  5,  6,  7,   |
  // [0,  1,  2,  3,   o-----> increasing x
  Tile = function(heightData, scale) {

    if (!this.isDataValid(heightData)) {
      return;
    };

    this.heightData = heightData || new Float32Array( 17 * 17 );
    this.size = Math.sqrt( this.heightData.length ) || 17;
    this.scale = scale || 1;

    this.geometry = new THREE.Geometry();

    this.setHeightData(this.heightData);
    this.computeGeometry();
  };

  // Methods
  Tile.prototype = {
    constructor: Tile,

    isDataValid: function(data) {
      var size = Math.sqrt( data.length );
      if ( data.length % size !== 0 ) {
        // NOT a square... bad juju
        console.log('Can\'t set Tile.heightData to non-square data!');
        return false;
      }
      return true;
    },

    getHeightData: function() { return this.heightData; },
    setHeightData: function(heightData) {

      if (!this.isDataValid(heightData)) {
        return;
      }

      this.size = Math.sqrt( this.heightData.length );
      this.heightData = heightData;

      // Think 1 square at a time, 2 triangles per square
      this.vertices = new Float32Array( 3*3*2*(this.size-1)*(this.size-1) );
      var scale = this.scale;
      for (var i = 0; i < this.size - 1; i++) {
        for (var j = 0; j < this.size - 1; j++) {

          var pts = [];
          pts.push([scale*j,     this.heightData[i * this.size + j],         -scale*i]);
          pts.push([scale*(j+1), this.heightData[i * this.size + j + 1],     -scale*i]);
          pts.push([scale*j,     this.heightData[(i+1) * this.size + j],      scale*(-i-1)]);
          pts.push([scale*(j+1), this.heightData[(i+1) * this.size + j + 1],  scale*(-i-1)]);

          // Triangle 1 (bottom left)
          this.vertices[((this.size-1)*i + j)*18 + 0] = pts[0][0];
          this.vertices[((this.size-1)*i + j)*18 + 1] = pts[0][1];
          this.vertices[((this.size-1)*i + j)*18 + 2] = pts[0][2];
          this.vertices[((this.size-1)*i + j)*18 + 3] = pts[1][0];
          this.vertices[((this.size-1)*i + j)*18 + 4] = pts[1][1];
          this.vertices[((this.size-1)*i + j)*18 + 5] = pts[1][2];
          this.vertices[((this.size-1)*i + j)*18 + 6] = pts[2][0];
          this.vertices[((this.size-1)*i + j)*18 + 7] = pts[2][1];
          this.vertices[((this.size-1)*i + j)*18 + 8] = pts[2][2];
          // Triangle 2 (top right)
          this.vertices[((this.size-1)*i + j)*18 + 9]  = pts[1][0];
          this.vertices[((this.size-1)*i + j)*18 + 10] = pts[1][1];
          this.vertices[((this.size-1)*i + j)*18 + 11] = pts[1][2];
          this.vertices[((this.size-1)*i + j)*18 + 12] = pts[3][0];
          this.vertices[((this.size-1)*i + j)*18 + 13] = pts[3][1];
          this.vertices[((this.size-1)*i + j)*18 + 14] = pts[3][2];
          this.vertices[((this.size-1)*i + j)*18 + 15] = pts[2][0];
          this.vertices[((this.size-1)*i + j)*18 + 16] = pts[2][1];
          this.vertices[((this.size-1)*i + j)*18 + 17] = pts[2][2];
        }
      }
    },

    getVertices: function() { return this.vertices; },

    getSize: function() { return this.size; },

    computeGeometry: function() {
      var geometry = new THREE.Geometry();

      for ( var i = 0; i < this.vertices.length; i+=3) {
        // For each vertex...
        geometry.vertices.push( new THREE.Vector3( this.vertices[i],
                                                   this.vertices[i+1],
                                                   this.vertices[i+2]
                                                   ));
      }

      for (var i = 0; i < this.vertices.length / 3; i+=3) {
        // For each triangle...
        geometry.faces.push( new THREE.Face3( i, i+1, i+2 ));
      }
      geometry.computeBoundingSphere();
      geometry.computeFaceNormals();
      geometry.computeVertexNormals();

      this.geometry = geometry;
    },

    getGeometry: function() { return this.geometry; },

    getMesh: function() {

      //var material = new THREE.MeshLambertMaterial( { wireframe: true, color: 0x00ff00 } );

      return new THREE.Mesh( this.geometry, _material.ground );
    }
  };

  return Tile;
});