define(["threejs", "Camera", "Light"], function(THREE, _camera, _light) {
  return {
    ground: new THREE.ShaderMaterial({
      wireframe: false,
      uniforms: {
      	lightPos: { type: "v3", value: new THREE.Vector3(1.0, 1.0, 1.0)},//_light.directional.position },
        lightAmb: { type: "c", value: new THREE.Color(0.1, 0.1, 0.1) },
        lightDiff: { type: "c", value: new THREE.Color(1.0, 1.0, 1.0) },
        matAmb: { type: "c", value: new THREE.Color(0.5, 1.0, 0.5) },
        matDiff: { type: "c", value: new THREE.Color(0.5, 1.0, 0.5) },
        fogParam: { type: "f", value: 0.001 }
      },
        
      vertexShader: document.getElementById('terrainVs').textContent,
      fragmentShader: document.getElementById('terrainFs').textContent
    }),
    cube: new THREE.MeshLambertMaterial({ color: 0xffffff })
  }
});