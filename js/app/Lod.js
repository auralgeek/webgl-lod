define(['threejs', 'Tile', 'PlayerCharacter', 'Material'], function(THREE, Tile, _playerCharacter, _material) {
  // Lod class

  // Constructor
  var Lod = function(lodLvls, tileSize, scale) {

    this.lodLvls = lodLvls;
    this.tileSize = tileSize || 16;
    this.scale = scale || 1;

    this.tiles = [];

    this.createTiles(this.lodLvls, this.tileSize, this.scale);
  };

  // Methods
  Lod.prototype = {
    constructor: Lod,

    genHeightData: function(size) {
      var heightData = new Float32Array(size*size);
      for (var i = 0; i < heightData.length; i++) {
        //heightData[i] = Math.random();
        heightData[i] = 0;
      }
      return heightData;
    },

    createTiles: function(lodLvls, tileSize, scale) {

      // Construct a single quad.
      var heightData = this.genHeightData(tileSize + 1)
      this.tiles = [new Tile(heightData, scale)];
      for (var i = 0; i < lodLvls; i++) {
        var newScale = Math.pow(2, i) * scale;
        heightData = this.genHeightData(tileSize + 1)
        aTile = new Tile(heightData, newScale);
        this.tiles.push(aTile);
      }
    },

    addMeshToScene: function(scene) {
      var geometry = new THREE.Geometry();
      for (var i = 0; i < this.tiles.length; i++) {

        // TODO: position translation here..
        var xoffset = 0;
        var yoffset = -3;
        var zoffset = 0;
        if (i > 0) {

          var offset = (this.tiles[0].size - 1) * this.tiles[0].scale * Math.pow(2, i-1);

          var geos = [];
          for (var j = 0; j < 12; j++) {
            geos.push( this.tiles[i].getGeometry().clone() );
          }

          var meshes = [];
          geos.map(function(geo) {
            meshes.push( new THREE.Mesh(geo) );
          });

          // First of 12
          meshes[0].position.set(xoffset, yoffset, zoffset - offset);
          meshes[1].position.set(xoffset + offset, yoffset, zoffset - offset);
          meshes[2].position.set(xoffset + offset, yoffset, zoffset);

          meshes[3].position.set(xoffset + offset, yoffset, zoffset + offset);
          meshes[4].position.set(xoffset + offset, yoffset, zoffset + 2*offset);
          meshes[5].position.set(xoffset, yoffset, zoffset + 2*offset);

          meshes[6].position.set(xoffset - offset, yoffset, zoffset + 2*offset);
          meshes[7].position.set(xoffset - 2*offset, yoffset, zoffset + 2*offset);
          meshes[8].position.set(xoffset - 2*offset, yoffset, zoffset + offset);

          meshes[9].position.set(xoffset - 2*offset, yoffset, zoffset);
          meshes[10].position.set(xoffset - 2*offset, yoffset, zoffset - offset);
          meshes[11].position.set(xoffset - offset, yoffset, zoffset - offset);

          meshes.map(function(mesh) {
            mesh.updateMatrix();
          });

          meshes.map(function(mesh) {
            geometry.merge(mesh.geometry, mesh.matrix);
          })

          scene.add(new THREE.Mesh(geometry, _material.ground));

        } else {
          // Central quad
          var offset = (this.tiles[i].size - 1) * this.tiles[i].scale;

          var geos = [];
          for (var j = 0; j < 4; j++) {
            geos.push( this.tiles[0].getGeometry().clone() );
          }

          var meshes = [];
          geos.map(function(geo) {
            meshes.push( new THREE.Mesh(geo) );
          });

          meshes[0].position.set(xoffset, yoffset, zoffset);
          meshes[1].position.set(xoffset, yoffset, zoffset + offset);
          meshes[2].position.set(xoffset - offset, yoffset, zoffset + offset);
          meshes[3].position.set(xoffset - offset, yoffset, zoffset);

          meshes.map(function(mesh) {
            mesh.updateMatrix();
          });

          meshes.map(function(mesh) {
            geometry.merge(mesh.geometry, mesh.matrix);
          })

          scene.add(new THREE.Mesh(geometry, _material.ground));
        }
      }
    }
  };
  return Lod;
});