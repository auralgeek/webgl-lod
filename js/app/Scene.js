define(['threejs'], function(THREE) {
	_scene = new THREE.Scene();
	_scene.fog = new THREE.FogExp2( 0xffffff, 0.001 );

	_scene.add( new THREE.AxisHelper(40) );
	return _scene;
});