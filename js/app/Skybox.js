requirejs(['threejs', 'Scene', 'SkyShader'], function(THREE, _scene, Sky) {

	// TODO: Refine this
	var sky = new THREE.Sky();
	sky.uniforms.sunPosition.value = new THREE.Vector3(700000, 100000, 700000);
	_scene.add(sky.mesh);

	// Add the sun
	var sunSphere = new THREE.Mesh(
		new THREE.SphereGeometry( 20000, 16, 8 ),
		new THREE.MeshBasicMaterial( { color: 0xffffff } )
	);
	sunSphere.position.x = 700000;
	sunSphere.position.y = 100000;
	sunSphere.position.z = 700000;
	sunSphere.visible = false;
	_scene.add(sunSphere);

	return {
		sun: sunSphere,
		sky: sky
	}
});