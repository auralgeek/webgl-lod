define(['threejs', 'Scene'], function(THREE, _scene) {
    var _light = {
    	ambient: new THREE.AmbientLight(0xffffff, 0.1),
    	directional: new THREE.DirectionalLight(0xffffbb, 1.0)
    };

    _light.directional.position.set(7, 1, 7);

    _scene.add(_light.ambient);
    _scene.add(_light.directional);

    return _light;
});