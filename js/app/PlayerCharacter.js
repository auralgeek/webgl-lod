define(['threejs', 'Scene', 'Camera', 'Keyboard', 'Material'], function(THREE, _scene, _camera, _keyboard, _material) {
	// Load in PC model...
    var loader = new THREE.JSONLoader();

    /*loader.load('xwing.js', function(geometry) {
        console.log('loading x-wing...');
        _playerCharacter = new THREE.Mesh(geometry, new THREE.MeshFaceMaterial());
        _playerCharacter.position.set(0, 80, 0);
        scene.add(_playerCharacter);
        pcDoneLoading = 1;
        console.log('Done!');
        animate();
    });*/
    var geometry = new THREE.BoxGeometry(1, 1, 1);
    mesh = new THREE.Mesh(geometry, _material.cube);
    mesh.position.set(0, 10, 0);
    pcDoneLoading = 1;
    _scene.add(mesh);

    var _playerCharacter = {
    	mesh: mesh,
	    update: function(deltat) {
		    var moveDist = 20 * deltat;
		    var rotateAngle = Math.PI / 2 * deltat;

		    // Shift is fast move.
		    if (_keyboard.pressed('shift')) {
		        moveDist = 3 * moveDist;
		        rotateAngle = 3 * rotateAngle;
		    }

		    // Move fwd/bckwd/left/right
		    if (_keyboard.pressed('w')) {
		        _playerCharacter.mesh.translateZ(-moveDist);
		    }
		    if (_keyboard.pressed('s')) {
		        _playerCharacter.mesh.translateZ(moveDist);
		    }
		    if (_keyboard.pressed('a')) {
		        _playerCharacter.mesh.translateX(-moveDist);
		    }
		    if (_keyboard.pressed('d')) {
		        _playerCharacter.mesh.translateX(moveDist);
		    }

		    // Rotate left/right/up/down.
		    if (_keyboard.pressed('q')) {
		        _playerCharacter.mesh.rotateOnAxis(new THREE.Vector3(0, 1, 0), rotateAngle);
		    }
		    if (_keyboard.pressed('e')) {
		        _playerCharacter.mesh.rotateOnAxis(new THREE.Vector3(0, 1, 0), -rotateAngle);
		    }
		    if (_keyboard.pressed('r')) {
		        _playerCharacter.mesh.rotateOnAxis(new THREE.Vector3(1, 0, 0), rotateAngle);
		    }
		    if (_keyboard.pressed('f')) {
		        _playerCharacter.mesh.rotateOnAxis(new THREE.Vector3(1, 0, 0), -rotateAngle);
		    }

		    // Reset location key.
		    if (_keyboard.pressed('z')) {
		        _playerCharacter.mesh.position.set(0, 5, 0);
		        _playerCharacter.mesh.rotation.set(0, 0, 0);
		    }

		    // Wire mesh toggle
		    if (_keyboard.pressed('m')) {
		    	console.log('pressed m');
		    	if ( _material.ground.wireframe == false ) {
					_material.ground.wireframe = true;
					console.log('true');
		    	} else {
		    		_material.ground.wireframe = false;
		    		console.log('false');
		    	}
		    }

		    // Force up
		    //console.log(_scene.children[4].geometry.vertices[0].y);
		    //if (_scene.mesh.)

		    var relativeCameraOffset = new THREE.Vector3(0, 4, 18);
		    var cameraOffset = relativeCameraOffset.applyMatrix4(_playerCharacter.mesh.matrixWorld);

		    _camera.position.set(cameraOffset.x, cameraOffset.y, cameraOffset.z);
		    _camera.lookAt(_playerCharacter.mesh.position);
		}	
    }
    
	return _playerCharacter;
});