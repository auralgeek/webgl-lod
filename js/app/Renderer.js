define(['threejs'], function(THREE) {
	
	var WIDTH = window.innerWidth;
    var HEIGHT = window.innerHeight;
	
	_renderer = new THREE.WebGLRenderer({antialias: true});
    _renderer.setSize(WIDTH, HEIGHT);
    document.body.appendChild(_renderer.domElement);
    _renderer.setClearColor(0x000000);

	window.addEventListener('resize', function() {
        _renderer.setSize(WIDTH, HEIGHT);
    });

    return _renderer;
});