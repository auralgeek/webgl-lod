define(["threejs", "Scene", "Camera", "Renderer", "Light", "Lod", "PlayerCharacter", "Material", "Skybox"],
function(THREE, _scene, _camera, _renderer, _light, Lod, _playerCharacter, _material, _skybox) {
	var app = {
		clock: new THREE.Clock(),
		init: function() {

		    ////////////////////
		    // Add geometry here
		    var geometry = new THREE.BoxGeometry(1, 1, 1);
		    var material = new THREE.MeshLambertMaterial({color: 0x00ff00});
		    var cube = new THREE.Mesh(geometry, material);
		    _scene.add(cube);

		    // Testing around with my Lod class
		    // Lod(# LODs, # squares / tile, square scale)
		    var lod = new Lod(4, 32, 1.0);
		    lod.addMeshToScene(_scene);
		},
		animate: function() {
		    requestAnimationFrame(app.animate);

		    // Render the scene
		    _renderer.render(_scene, _camera);
		    var deltat = app.clock.getDelta();
		    _playerCharacter.update(deltat);
		    //_controls.update(deltat);
		}
	};
	return app;
});