# webgl-lod

This is a simple implementation of LOD in WebGL.  Basically sets up a flat quadtree structured mesh in js (using [Three.js](http://threejs.org/) helper library) and then applies a few octaves of simplex noise in the vertex shader on the GPU based on the camera position.

# Controls

Move around with standard `wasd` controls, turn left or right with `q` or `e` respectively.  Hold `shift` to go faster.  `z` resets your position.

# Inspiration

Borrowed code structure from [this](https://github.com/felixpalmer/lod-terrain) example.  I really liked how modular his use of [requirejs](http://requirejs.org/) made everything.  I think that will be extremely helpful to me going forward.  Also referencing at least the water shaders from [this](https://github.com/jwagner/terrain/) project.

# Going Forward

So what I want to eventually have is have an interesting terrain you can run around on.  Next steps are:

- Implement some procedural texturing to our shaded surfaces.
- Instead of using naive square tiles on the large scale I'd like to use a quadtree structure that uses distance to denote LOD transition points, this would approximate the shape of concentric circles about the camera position rather than simple squares.
- Figure out a sane structure for managing shaders... keeping them all in html is a PITA, but I like that I don't need a server running to develop.
- Code getting a bit messy, clean it up, keep it super modular so each file is nice and simple.
- Figure out if running multiple renders per frame is valuable to me... seems like having a pre render that spits out a texture which can then be used by other shaders could be a way to 'layer' my shaders.
- (Optional) Set the 'character' cube mesh on the ground rather than floating around.  This might be too jarring though without making the terrain less.... uh... 'rugged.'
- (Optional) Get a _real_ mesh for the 'character', because cubes aren't really that great.
- (Optional) Walking/running animation for said character?
- (Optional) Clouds? Clouds.
- (Optional) Billboard instanced grass mebbe?
- (Optional) Create some flora models offline using L-systems?
- (Optional) Play with that noise function!... I'd like some regional variation... I think this means take what I have and throw in some kind of multiplier factor with several octave lower frequency.

Clearly I have some work to do, yet.